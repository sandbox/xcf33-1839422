<?php

/**
 * Variables available
 * (int) $unlocked_count
 * (array) $next
 *    description (string, html)
 *    title (string)
 *    data (array)
 */
?>
<?php
$completion_percentage = number_format($unlocked_count / $total_count, 2) * 100;
?>
<div id="profile_completion_progress_block">
<a href="#" class="close"></a>
  <h3>Your profile completion</h3>
<div id="progress_bar" class="ui-progress-bar ui-container transition">
            <div style="width: <?php print $completion_percentage; ?>%;" class="ui-progress">
              <span style="display: none;" class="ui-label">Completed</span>
            </div>
          </div>
  <div class="count">You have completed <?php print $unlocked_count; ?> out of <?php print $total_count; ?> profile items</div>
  <div class="description"><?php print $next['description']; ?></div>
  
</div>