<?php

function profile_completion_preprocess_block_progress(&$variables) {
  
  $path = drupal_get_path('module', 'profile_completion');
  
  drupal_add_js($path . '/profile_completion.js');  
  drupal_add_css($path . '/profile_completion.css');  
}