<?php

/**
 * Implements hook_menu
 */
function profile_completion_menu() {

  // adding one of many profile completion components
  $items['admin/structure/profile_completion/add'] = array(
    'title' => 'Add a new profile completion component',
    'description' => 'Adding new profile completion component',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_completion_component_add_form'),
    'access arguments' => array('administer profile completion'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  // adding one of many profile completion components
  $items['admin/config/people/profile_completion'] = array(
    'title' => 'Profile Completion',
    'description' => 'Configuration for profile completion',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_completion_config_form'),
    'access arguments' => array('administer profile completion'),
    'type' => MENU_NORMAL_ITEM,
  );   
  
  // need a simple configuration page with prefix, etc
  
  // menu callback for all the components declared in other modules
  $components = profile_completion_components_load();
  if (!empty($components)) {    
    foreach($components as $name => $info) {
      $items['admin/structure/profile_completion/add/' . $name] = array(
        'title' => 'Add new ' . $info['label'] . ' to be part of profile completion',
        'description' => $info['description'],
        'page callback' => 'profile_completion_component_form',
        'page arguments' => array($info['module'], $name),
        'access arguments' => array('administer profile completion'),
        'type' => MENU_CALLBACK,
      );        
    
      $items['admin/structure/profile_completion/' . $name . '/%profile_completion'] = array(
        'title' => 'Edit ' . $info['label'],
        'description' => $info['description'],
        'page callback' => 'profile_completion_component_form',
        'page arguments' => array($info['module'], $name, 4),
        'access arguments' => array('administer profile completion'),
        'type' => MENU_CALLBACK,
      );
      
      $items['admin/structure/profile_completion/' . $name . '/%profile_completion/delete'] = array(
        'title' => 'Delete ' . $info['label'],
        'description' => $info['description'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('profile_completion_component_delete_form', 4),
        'access arguments' => array('administer profile completion'),
        'type' => MENU_CALLBACK,
      );
      
    }  
  }  
  return $items;  
}

/**
 * Implements hook_views_api().
 */
function profile_completion_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'profile_completion') . '/includes/views',
  );
}


function profile_completion_main() {
  return 'this is a stub right now, later will probably be a view of tables of different profile completion component';  
}


/**
 * Implements hook_permission
 */
function profile_completion_permission() {
  return array(
    'administer profile completion' => array(
      'title' => t('Administer Profile Completion'),
      'description' => t('Manage profile completion components and configuration'),
    ),
  );  
}

/**
 * Implements hook_theme
 */
function profile_completion_theme() {
 $path = drupal_get_path('module', 'profile_completion');
 $theme_inc = 'theme.inc';
  
  return array(
    'profile_completion_block_progress' => array(
        'path' => $path,
        'template' => 'profile-completion-block-progress',
        'preprocess functions' => array(
          'template_preprocess',
          'profile_completion_preprocess_block_progress',
        ),
        'file' => $theme_inc,    
    ),
  );
}

/*
 * Implementation of hook_token_info
 */ 
function profile_completion_token_info() {
	
	$info['types']['global'] = array(
		'name' => t('global information'),
		'description' => t('Global information'),
	);
		
	return $info;
}

/*
 * Implementation of hook_tokens
 */
function profile_completion_tokens($type, $tokens, array $data = array(), array $options = array()) {
}

/**
 * Implements hook_block_info().
 */
function profile_completion_block_info() {
  return array(
    'profile_completion_progress' => array(
      'info'  => t('Profile completion: progress'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function profile_completion_block_view($delta = '') {
    switch($delta) {
      case 'profile_completion_progress':
        $block['content'] = profile_completion_progress_block();
        $block['subject'] = '';
      break;
    }
    
    return $block;
}

/**
 * Access check for profile completion block 
 */
function _profile_completion_progress_block_access() {
  return user_access('earn achievements');
  
}

/**
 * Show user the profile completion items they have achieved and ones they are still missing
 * this block will appear on the attorney profile page
 */
function profile_completion_progress_block() {  
  
  if (!_profile_completion_progress_block_access()) {
    return;  
  }
  
  global $user;
  
  // Load all achievements.
  $achievements = achievements_load();
  $pc_achievements = array();
  $locked = array();
  foreach ($achievements as $achievement_id => $achievement) {
    // let's just get the profile completion achievements
    if ($achievement['group'] == variable_get('profile_completion_prefix', 'profile')) {
      $pc_achievements[$achievement_id] = $achievement;  
    }
  }
  $total_count = count($pc_achievements);
  
  foreach($pc_achievements as $id => $info) {
    if (!achievements_unlocked_already($id, $user->uid)) {
      $locked[$id] = $info;
    }
  }
   $locked_count = count($locked);
   $unlocked_count = $total_count - $locked_count;
   
   if (empty($locked)) {
     return;  
   }
   
   $next_todo = array_rand($locked);
   
   // @todo: find the profile content (1st one) that belongs to the user
   // when we find it, we will add destination to the description link
   // if not, no destination link
   	$query = db_select('node', 'n');
  	$query
  	->condition('n.type', variable_get('profile_completion_content_type', 'page'))
  	->condition('n.uid', $user->uid)
  	->fields('n');
		$node = $query->execute()->fetchObject();
    
    if (empty($node)) {
      return;  
    }
    // we are assuming the progress bar block can really appear in the profile content
    if ($current_node = menu_get_object()) {
      if ($current_node->nid != $node->nid) {
         return; 
       }
    }
    
    $options = array();
    if (!empty($node)) {
      $destination = drupal_get_path_alias("node/{$node->nid}");
      $options['query']['destination'] = $destination;
    }
    
   // make the entire description a link
   if (isset($locked[$next_todo]['action_link'])) {
     $locked[$next_todo]['action_link'] = token_replace($locked[$next_todo]['action_link']);
     $locked[$next_todo]['description'] = l($locked[$next_todo]['description'], $locked[$next_todo]['action_link'], $options);
   }
  
  return theme('profile_completion_block_progress', array('total_count' => $total_count, 'unlocked_count' => $unlocked_count, 'next' => $locked[$next_todo]));
  
}

/**
 * Implements hook_achievement_info()
 */
function profile_completion_achievements_info() {
  $achievements = array();
  
  // give the opportunity for other modules to add their own achievements (profile completion components to the list)
  $achievements = module_invoke_all('profile_completion_achievement');
  return $achievements;
}

/**
 * Configuration form
 */
function profile_completion_config_form($form, &$form_state) {
  
  $form['profile_completion_prefix'] = array(
    '#type' => 'textfield',
    '#title' => 'Profile completion achievement group',
    '#description' => t('The group name the profile completion components will be used, 99% time you can leave it'),
    '#default_value' => variable_get('profile_completion_prefix', 'profile'),
  );
  
    $_node_types = node_type_get_types();
  $options = array('' => t('--Select--'));
  foreach($_node_types as $node_type) {
    $options[$node_type->type] = $node_type->name;  
  }
  
  $form['profile_completion_content_type'] = array(
    '#type' => 'select',
    '#title' => 'Profile content type',
    '#description' => t('The content type user profile will be built on.'),
    '#options' => $options,
    '#default_value' => variable_get('profile_completion_content_type', 'page'),
  );
  
  
  return system_settings_form($form);
}

/**
 * Add a new profile completion component
 */
function profile_completion_component_add_form($form, &$form_state) {
  
  // let's load all the components available
  $components = profile_completion_components_load();  
  
  // no components
  if (empty($components)) {
    $form['message'] = array(
      '#type' => 'item',
      '#markup' => 'It appears you have no profile completion components enabled, please go to your module list and enable them',
      '#prefix' => '<div class="messages warning">',
      '#suffix' => '</div>',
    ); 
    return $form;  
  }
  
  // build a checkbox select list of the components and it should just redirect them to the component adding form which the component module will define
  $options = array();
  foreach($components as $name => $info) {
    $options[$name] = $info['label'];  
  }
  
  $form['components'] = array(
    '#type' => 'radios',
    '#title' => 'Profile Completion Component',
    '#description' => t('Please choose one of the component below that will be part of the profile completion'),
    '#options' => $options,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Next',
  );
  
  return $form;
}

/**
 * Submission handler
 */
function profile_completion_component_add_form_submit($form, &$form_state) {
  
  // redirect them to the component adding form, we should create a callback function so that 
  // the component module needs only to create the form and its handlers
  $form_state['redirect'] = 'admin/structure/profile_completion/add/' . $form_state['values']['components'];  
}

/**
 * Form callback for modules that declares its own component for profile completion
 */
function profile_completion_component_form($module_name, $component_name, $profile_completion = NULL) {
  // important that the component module declares this form in the consistent format
  return drupal_get_form("{$module_name}_{$component_name}_form", $profile_completion);  
}

/** 
 * Delete a component from the profile completion
 */
function profile_completion_component_delete_form($form, &$form_state, $profile_completion) {
  // confirm form
  $form['profile_completion_name'] = array(
    '#type' => 'value',
    '#value' => $profile_completion['name'],
  );
    
  $question = t('Are you sure you want to delete the @component?', array('@component' => $profile_completion['label']));
  $path = 'admin/structure/profile_completion';
  
  return confirm_form($form, $question, $path, NULL, t('Delete'), t('Cancel'));
 
}

/**
 * Submission callback
 */
function profile_completion_component_delete_form_submit($form, &$form_state) {
  
  db_delete('profile_completion')
   ->condition('name', $form_state['values']['profile_completion_name'])
   ->execute();	
  
  $form_state['redirect'] = 'admin/structure/profile_completion';
  drupal_set_message(t('The component has been deleted'));
}

/**
 * Returns common form elements to be used on all profile completion forms
 */
function _profile_completion_component_form($component_name, $profile_completion = NULL) {
   
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => 'Name',
    '#description' => 'Please enter the profile completion name',
    '#required' => TRUE,
    '#default_value' => (isset($profile_completion)) ? $profile_completion['label'] : '',
  );
 
 $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 64,
    '#machine_name' => array(
      'exists' => '_profile_completion_machine_name_check',
      'source' => array('label'),
    ),
   '#default_value' => (isset($profile_completion)) ? $profile_completion['name'] : '',
   '#disabled' => (isset($profile_completion)) ? TRUE : FALSE,
  );

  $form['component'] = array(
    '#type' => 'value',
    '#value' => check_plain($component_name),  
  );
  
  $form['action_link'] = array(
    '#type' => 'textfield',
    '#title' => 'Action Path',
    '#description' => t('The path to take the action and complete this profile component, i.e. node/add/page, you can also use token replacement below'),
    '#default_value' => (isset($profile_completion)) ? $profile_completion['action_link'] : '',
  );

	$form['token_help'] = array(
		'#title' => t('Replacement Tokens'),
		'#token_types' => array('global'),
		'#theme' => 'token_tree',
	);
  
  
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => 'Description',
    '#description' => t('Enter the instruction on how to complete this part of the profile, this should only contain plain text'),
    '#default_value' => (isset($profile_completion)) ? $profile_completion['description'] : '',
    // '#element_validate' => array('_profile_completion_description_validate'),
  );
  
  return $form;
}

function _profile_completion_machine_name_check(){}


/**
 * Load the component achievement information and return the achievements array that can be added to
 * hook_achievements_info()
 */
function _profile_completion_component_achievements_load($component_name) {
  $achievements = array();
  
  $conditions = array(
    array(
      'field' => 'component',
      'value' => check_plain($component_name),
    ),
  );
  
  $profile_completions = array();
  $profile_completions = profile_completion_load_all($conditions);
  // construct the achievement info
  // @todo: might need prefix them later
  foreach($profile_completions as $profile_completion) {
    $achievements[$profile_completion['name']] = array(
      'title' => $profile_completion['label'],
      'description' => strip_tags($profile_completion['description']),
      'action_link' => $profile_completion['action_link'],
      'points' => 1,
      'group' => variable_get('profile_completion_prefix', 'profile'),
    );
    
    if (isset($profile_completion['image_locked'])) {
      $achievements[$profile_completion['name']]['images']['locked'] = $profile_completion['image_locked'];
    }
    
    if (isset($profile_completion['image_unlocked'])) {
      $achievements[$profile_completion['name']]['images']['unlocked'] = $profile_completion['image_locked'];
    }

    if (isset($profile_completion['image_hidden'])) {
      $achievements[$profile_completion['name']]['images']['hidden'] = $profile_completion['image_hidden'];
    }
    
    // achievements data
    $achievements[$profile_completion['name']]['data'] = unserialize($profile_completion['data']);
      
  }
  
  return $achievements;
    
}

/**
 * Profile completion component load
 * load all the components declared by other modules
 */
function profile_completion_components_load() {
  $components = array();
  $components = module_invoke_all('profile_completion_component');
  
  return $components;  
}

/**
 * Saves a profile completion component entry
 */
function profile_completion_save($record) {
  $profile_completion_component = profile_completion_load($record['name']);
  if (!empty($profile_completion_component)) {
    drupal_write_record('profile_completion', $record, 'name');  
  } 
  else {
    drupal_write_record('profile_completion', $record);
  } 
}

/**
 * Loads a profile completion component entry
 */
function profile_completion_load($machine_name) {
  
  $data = array();
  $query = db_select('profile_completion', 'p')
  ->fields('p')
  ->condition('name', $machine_name);

  $result = $query->execute();
  $data = $result->fetchAssoc();

  return $data;  
}

/**
 * Loads all profile completion component entry
 */
function profile_completion_load_all($conditions = array()) {
  $data = array();
  $query = db_select('profile_completion', 'p')
  ->fields('p');
  
  if (!empty($conditions)) {
    foreach($conditions as $condition) {
      if (!array_key_exists('operator', $condition)) {
        $condition['operator'] = '=';  
      }
      $query->condition($condition['field'], $condition['value'], $condition['operator']);
    }  
  }
  $result = $query->execute();
  while($record = $result->fetchAssoc()) {
    $data[] = $record;  
  }

  return $data;    
}
