<?php

/**
 * @file
 * Export CRM Core Profile to views.
 */


/**
 * Implements hook_views_data().
 */
function profile_completion_views_data() {
  $data = array();

  $data['profile_completion']['table']['group'] = t('Profile Completion');
  $data['profile_completion']['table']['base'] = array(
    'field' => 'name',
    'title' => t('Profile Completion'),
    'help' => t('Machine name of the profile completion component'),
  );

  $data['profile_completion']['name'] = array(
    'title' => t('Component Name'),
    'help' => t('Machine name of the profile completion component'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['profile_completion']['label'] = array(
    'title' => t('Label'),
    'help' => t('The human-readable name of the type of the component.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['profile_completion']['description'] = array(
    'title' => t('Description'),
    'help' => t('Description of the component'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
   );
   
  $data['profile_completion']['label'] = array(
    'title' => t('Label'),
    'help' => t('The human-readable name of the type of the component.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['profile_completion']['component'] = array(
    'title' => t('Component Type'),
    'help' => t('The type of component'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['profile_completion']['created'] = array(
    'title' => t('Created date'),
    'help' => t('The date the component was first added.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  return $data;
}